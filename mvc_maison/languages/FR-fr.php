<?php
//isoler ici dans des constantes les textes affichés sur le site
define('LOGO', 'Logo de la compagnie'); // Affiché si image non trouvée

define('MENU_ACCUEIL','Accueil');
define('MENU_PAGE_1','page_1');
define('MENU_PAGE_2','page_2');
define('MENU_PAGE_3','page_3');
define('TEXTE_PAGE_404','Oops, la page demandée n\'existe pas !');
define('MESSAGE_ERREUR',"Une erreur s'est produite");
define('TITRE_PAGE_ACCUEIL_TOUS','Accueil');
define('AUTHENF','Page authentification');
define('TITRE', 'Intranet');
define('ERREUR_QUERY', 'Problème d\'accès à la base de données. Contactez l\'administrateur');

