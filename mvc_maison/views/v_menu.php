<?php
/*
 * Copyright 2016, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * menu: http://www.w3schools.com/bootstrap/bootstrap_ref_comp_navs.asp
 */
?>
<!-- Menu du site -->

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <ul class="nav navbar-nav">

				<li <?php echo ($page=='page_1' ? 'class="active"':'')?>>
					<a href="index.php?page=page_1">
						<?= MENU_PAGE_1 ?>
					</a>
				</li>
				<li <?php echo ($page=='page_2' ? 'class="active"':'')?>>
					<a href="index.php?page=page_2">
						<?= MENU_PAGE_2 ?>
					</a>
				</li>
				<li <?php echo ($page=='page_3' ? 'class="active"':'')?>>
					<a href="index.php?page=page_3">
						<?= MENU_PAGE_3 ?>
					</a>
				</li>
    </ul>
  </div>
</nav>


